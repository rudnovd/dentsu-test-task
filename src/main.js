import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import store from './store'

Vue.config.productionTip = false
Vue.use(Vuetify)

const vuetify = new Vuetify({
  icons: {
    iconfont: 'mdiSvg',
  },
})

new Vue({
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
