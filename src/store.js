import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state() {
    return {
      items: [],
      selected: {
        index: -1,
        text: '',
      },
    }
  },
  mutations: {
    create(state, item) {
      state.items.push({
        index: state.items.length ? state.items[state.items.length - 1].index + 1 : 0,
        text: item.text,
      })
      state.selected.text = ''
    },
    select(state, index) {
      state.selected = {
        index,
        text: state.items[index].text,
      }
    },
    edit(state, payload) {
      state.selected = {
        index: -1,
        text: '',
      }
      state.items[payload.index].text = payload.text
    },
    delete(state, index) {
      state.items.splice(index, 1)
    },
  },
})
